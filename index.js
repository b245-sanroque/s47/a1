console.log("JS DOM Manipulation")

//[SECTION] : Document Object Manipulation
	// allows us to acces or modify the properties of an html element in a webpage
	// it is standard on how to get, change, add or delete HTML elements
	// we will be focusing only with DOM in terms of managing forms

	// For selecting HTML elements we will be using document.querySelector / getElementById
		// SYTAX: document.querySelector("html element")
			// CSS Selector
				//    - ID selector
 				//    - Class selector
 				//    - Attribute selector
 				//    - Tag selector
 				//    - Universal selector

	//querySelectorAll - all
	//querySelector - first one to find

	//universal
	let universalSelector = document.querySelectorAll("*");
	let singleUniversalSelector = document.querySelector("*");
	console.log("universalSelector");
	console.log(universalSelector);
	console.log("singleUniversalSelector");
	console.log(singleUniversalSelector);

	//class
	let classSelector = document.querySelectorAll(".full-name");
	let singleClassSelector = document.querySelector(".full-name");
	console.log("classSelector");
	console.log(classSelector);
	console.log("singleClassSelector");
	console.log(singleClassSelector);

	let idSelector = document.querySelector("#txt-first-name");
	console.log("idSelector");
	console.log(idSelector);

	let tagSelector = document.querySelectorAll("input");
	console.log("tagSelector");
	console.log(tagSelector);

	// let spanSelector = document.querySelectorAll("span[id]"); //returns an array
	let spanSelector = document.querySelector("span[id]");
	console.log("spanSelector");
	console.log(spanSelector);

	//getElement
		let element = document.getElementById("fullName");
		console.log("getELementById");
		console.log(element);
		//if ByClass (with s) getElements
		element = document.getElementsByClassName("full-name");
		console.log("getElementsByClass");
		console.log(element);


// [SECTION] Event Listeners
	// whenever a user interacts with a webpage, this action is considered as an event
	// working with events is large part of creating interactivity in a webpage
	// specifif function that will be triggered if the event happened.

	// the function used is "addEventListener", it take two arguments
		// first, a string identifying the event
		// second, function that the listener will trigger once" specified event" occured


	let txtFirstName = document.querySelector("#txt-first-name");
	txtFirstName.addEventListener("keyup", ()=> {
		// console.log(txtFirstName.value);
		spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
		// spanSelector.innerHTML = `<h1>${txtFirstName.value}</h>`
	})
	let txtLastName = document.querySelector("#txt-last-name");
	txtLastName.addEventListener("keyup", ()=> {
		// console.log(txtLastName.value);
		spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
	})

	let color = document.querySelector("#text-color");
	console.log(color)

	color.addEventListener("click", () => {
		// console.log(color.value)
		spanSelector.innerHTML = `<font color = "${color.value}">${txtFirstName.value} ${txtLastName.value}</font>`
	})

	// color.addEventListener("change", () => {
	// 	console.log(color.value)
 	// 	spanSelector.innerHTML = `<font color = "${color.value}">${txtFirstName.value} ${txtLastName.value}</font>`
	// })
